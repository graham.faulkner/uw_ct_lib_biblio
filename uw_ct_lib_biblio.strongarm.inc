<?php
/**
 * @file
 * uw_ct_lib_biblio.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_lib_biblio_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_biblio';
  $strongarm->value = 'edit-workflow';
  $export['additional_settings__active_tab_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_annotations';
  $strongarm->value = 'none';
  $export['biblio_annotations'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_author_links';
  $strongarm->value = 1;
  $export['biblio_author_links'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_author_link_profile';
  $strongarm->value = 0;
  $export['biblio_author_link_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_author_link_profile_path';
  $strongarm->value = 'user/[user:uid]';
  $export['biblio_author_link_profile_path'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_auto_citekey';
  $strongarm->value = 1;
  $export['biblio_auto_citekey'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_auto_orphaned_author_delete';
  $strongarm->value = 0;
  $export['biblio_auto_orphaned_author_delete'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_base';
  $strongarm->value = 'bibliography';
  $export['biblio_base'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_baseopenurl';
  $strongarm->value = '';
  $export['biblio_baseopenurl'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_base_title';
  $strongarm->value = 'Bibliography';
  $export['biblio_base_title'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_button_hide';
  $strongarm->value = 1;
  $export['biblio_button_hide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_citekey_field1';
  $strongarm->value = 'nid';
  $export['biblio_citekey_field1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_citekey_field2';
  $strongarm->value = 'nid';
  $export['biblio_citekey_field2'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_citekey_phpcode';
  $strongarm->value = '';
  $export['biblio_citekey_phpcode'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_citekey_prefix';
  $strongarm->value = '';
  $export['biblio_citekey_prefix'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_citeproc_style';
  $strongarm->value = 'mla.csl';
  $export['biblio_citeproc_style'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_contrib_fields_delta';
  $strongarm->value = '2';
  $export['biblio_contrib_fields_delta'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_copy_taxo_terms_to_keywords';
  $strongarm->value = 0;
  $export['biblio_copy_taxo_terms_to_keywords'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_crossref_pid';
  $strongarm->value = '';
  $export['biblio_crossref_pid'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_display_citation_key';
  $strongarm->value = 0;
  $export['biblio_display_citation_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_download_links_to_node';
  $strongarm->value = 0;
  $export['biblio_download_links_to_node'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_export_links';
  $strongarm->value = array(
    'rtf' => 'rtf',
    'ris' => 'ris',
    'bibtex' => 0,
    'tagged' => 0,
    'marc' => 0,
    'xml' => 0,
  );
  $export['biblio_export_links'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_file_link_type';
  $strongarm->value = 'icon';
  $export['biblio_file_link_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_fix_isi_links';
  $strongarm->value = 0;
  $export['biblio_fix_isi_links'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_footnotes_integration';
  $strongarm->value = 0;
  $export['biblio_footnotes_integration'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_hide_bibtex_braces';
  $strongarm->value = 0;
  $export['biblio_hide_bibtex_braces'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_index';
  $strongarm->value = 1;
  $export['biblio_index'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_init_auth_count';
  $strongarm->value = '4';
  $export['biblio_init_auth_count'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_inpress_year_text';
  $strongarm->value = 'In Press';
  $export['biblio_inpress_year_text'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_isi_url';
  $strongarm->value = 'http://apps.isiknowledge.com/InboundService.do?Func=Frame&product=WOS&action=retrieve&SrcApp=EndNote&Init=Yes&SrcAuth=ResearchSoft&mode=FullRecord&UT=';
  $export['biblio_isi_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_keyword_freetagging';
  $strongarm->value = 0;
  $export['biblio_keyword_freetagging'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_keyword_orphan_autoclean';
  $strongarm->value = 1;
  $export['biblio_keyword_orphan_autoclean'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_keyword_sep';
  $strongarm->value = ',';
  $export['biblio_keyword_sep'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_keyword_vocabulary';
  $strongarm->value = '0';
  $export['biblio_keyword_vocabulary'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_last_ftdid';
  $strongarm->value = 217;
  $export['biblio_last_ftdid'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_links_target_new_window';
  $strongarm->value = 0;
  $export['biblio_links_target_new_window'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_link_title_url';
  $strongarm->value = 0;
  $export['biblio_link_title_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_lookup_links';
  $strongarm->value = array(
    'crossref' => 'crossref',
    'google' => 'google',
    'pubmed' => 0,
  );
  $export['biblio_lookup_links'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_my_pubs_menu';
  $strongarm->value = 0;
  $export['biblio_my_pubs_menu'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_node_layout';
  $strongarm->value = 'tabular';
  $export['biblio_node_layout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_no_year_text';
  $strongarm->value = 'Year unknown';
  $export['biblio_no_year_text'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_openurlimage';
  $strongarm->value = '';
  $export['biblio_openurlimage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_openurl_sid';
  $strongarm->value = 'Biblio:Library Content Reuse';
  $export['biblio_openurl_sid'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_order';
  $strongarm->value = 'ASC';
  $export['biblio_order'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_orphan_clean_interval';
  $strongarm->value = '86400';
  $export['biblio_orphan_clean_interval'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_orphan_clean_next_execution';
  $strongarm->value = 1406992334;
  $export['biblio_orphan_clean_next_execution'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_pm_age_limit';
  $strongarm->value = '2419200';
  $export['biblio_pm_age_limit'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_pm_auto_update';
  $strongarm->value = 0;
  $export['biblio_pm_auto_update'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_pm_dup_action';
  $strongarm->value = 'newrev';
  $export['biblio_pm_dup_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_pm_update_interval';
  $strongarm->value = '3600';
  $export['biblio_pm_update_interval'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_pm_update_limit';
  $strongarm->value = '100';
  $export['biblio_pm_update_limit'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_rowsperpage';
  $strongarm->value = '25';
  $export['biblio_rowsperpage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_rss';
  $strongarm->value = 1;
  $export['biblio_rss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_rss_number_of_entries';
  $strongarm->value = '100';
  $export['biblio_rss_number_of_entries'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_search';
  $strongarm->value = 1;
  $export['biblio_search'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_search_button_text';
  $strongarm->value = 'Search';
  $export['biblio_search_button_text'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_settings__active_tab';
  $strongarm->value = 'edit-sort';
  $export['biblio_settings__active_tab'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_show_crossref_profile_form';
  $strongarm->value = 1;
  $export['biblio_show_crossref_profile_form'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_show_openurl_profile_form';
  $strongarm->value = 0;
  $export['biblio_show_openurl_profile_form'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_show_profile';
  $strongarm->value = 0;
  $export['biblio_show_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_show_user_profile_form';
  $strongarm->value = 0;
  $export['biblio_show_user_profile_form'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_sort';
  $strongarm->value = 'title';
  $export['biblio_sort'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_sort_tabs';
  $strongarm->value = array(
    'author' => 'author',
    'title' => 'title',
    'type' => 'type',
    'year' => 'year',
    'keyword' => 0,
  );
  $export['biblio_sort_tabs'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_sort_tabs_style';
  $strongarm->value = 0;
  $export['biblio_sort_tabs_style'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_stop_words';
  $strongarm->value = 'a,an,is,on,the';
  $export['biblio_stop_words'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_style';
  $strongarm->value = 'classic';
  $export['biblio_style'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_user_style';
  $strongarm->value = 'system';
  $export['biblio_user_style'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'biblio_view_only_own';
  $strongarm->value = 0;
  $export['biblio_view_only_own'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_biblio';
  $strongarm->value = 0;
  $export['comment_anonymous_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_biblio';
  $strongarm->value = '1';
  $export['comment_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_biblio';
  $strongarm->value = 0;
  $export['comment_default_mode_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_biblio';
  $strongarm->value = '50';
  $export['comment_default_per_page_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_biblio';
  $strongarm->value = 0;
  $export['comment_form_location_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_biblio';
  $strongarm->value = '0';
  $export['comment_preview_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_biblio';
  $strongarm->value = 0;
  $export['comment_subject_field_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_biblio';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_biblio';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_biblio';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__biblio';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'entity_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '58',
        ),
        'redirect' => array(
          'weight' => '57',
        ),
        'xmlsitemap' => array(
          'weight' => '59',
        ),
        'biblio_type' => array(
          'weight' => '2',
        ),
        'biblio_year' => array(
          'weight' => '1',
        ),
        'biblio_authors_field' => array(
          'weight' => '4',
        ),
        'biblio_secondary_authors_field' => array(
          'weight' => '5',
        ),
        'biblio_tertiary_authors_field' => array(
          'weight' => '6',
        ),
        'biblio_subsidiary_authors_field' => array(
          'weight' => '7',
        ),
        'biblio_corp_authors_field' => array(
          'weight' => '8',
        ),
        'biblio_secondary_title' => array(
          'weight' => '10',
        ),
        'biblio_tertiary_title' => array(
          'weight' => '11',
        ),
        'biblio_volume' => array(
          'weight' => '12',
        ),
        'biblio_section' => array(
          'weight' => '15',
        ),
        'biblio_edition' => array(
          'weight' => '16',
        ),
        'biblio_issue' => array(
          'weight' => '13',
        ),
        'biblio_number_of_volumes' => array(
          'weight' => '14',
        ),
        'biblio_number' => array(
          'weight' => '17',
        ),
        'biblio_pages' => array(
          'weight' => '18',
        ),
        'biblio_date' => array(
          'weight' => '19',
        ),
        'biblio_publisher' => array(
          'weight' => '20',
        ),
        'biblio_place_published' => array(
          'weight' => '21',
        ),
        'biblio_type_of_work' => array(
          'weight' => '22',
        ),
        'biblio_lang' => array(
          'weight' => '23',
        ),
        'biblio_other_author_affiliations' => array(
          'weight' => '24',
        ),
        'biblio_isbn' => array(
          'weight' => '26',
        ),
        'biblio_issn' => array(
          'weight' => '25',
        ),
        'biblio_accession_number' => array(
          'weight' => '27',
        ),
        'biblio_call_number' => array(
          'weight' => '28',
        ),
        'biblio_other_number' => array(
          'weight' => '29',
        ),
        'biblio_keywords' => array(
          'weight' => '30',
        ),
        'biblio_abst_e' => array(
          'weight' => '31',
        ),
        'biblio_abst_f' => array(
          'weight' => '32',
        ),
        'biblio_notes' => array(
          'weight' => '33',
        ),
        'biblio_url' => array(
          'weight' => '34',
        ),
        'biblio_doi' => array(
          'weight' => '35',
        ),
        'biblio_research_notes' => array(
          'weight' => '36',
        ),
        'biblio_custom1' => array(
          'weight' => '37',
        ),
        'biblio_custom2' => array(
          'weight' => '38',
        ),
        'biblio_custom3' => array(
          'weight' => '39',
        ),
        'biblio_custom4' => array(
          'weight' => '40',
        ),
        'biblio_custom5' => array(
          'weight' => '41',
        ),
        'biblio_custom6' => array(
          'weight' => '42',
        ),
        'biblio_custom7' => array(
          'weight' => '43',
        ),
        'biblio_short_title' => array(
          'weight' => '44',
        ),
        'biblio_translated_title' => array(
          'weight' => '46',
        ),
        'biblio_alternate_title' => array(
          'weight' => '45',
        ),
        'biblio_original_publication' => array(
          'weight' => '47',
        ),
        'biblio_reprint_edition' => array(
          'weight' => '48',
        ),
        'biblio_citekey' => array(
          'weight' => '49',
        ),
        'biblio_coins' => array(
          'weight' => '50',
        ),
        'biblio_remote_db_name' => array(
          'weight' => '51',
        ),
        'biblio_remote_db_provider' => array(
          'weight' => '52',
        ),
        'biblio_auth_address' => array(
          'weight' => '53',
        ),
        'biblio_label' => array(
          'weight' => '54',
        ),
        'biblio_access_date' => array(
          'weight' => '55',
        ),
        'biblio_refereed' => array(
          'weight' => '56',
        ),
        'other_fields' => array(
          'weight' => '3',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'forward_display_biblio';
  $strongarm->value = 1;
  $export['forward_display_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_cancel_biblio';
  $strongarm->value = '0';
  $export['mb_content_cancel_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_sac_biblio';
  $strongarm->value = '0';
  $export['mb_content_sac_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_tabcn_biblio';
  $strongarm->value = 0;
  $export['mb_content_tabcn_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_biblio';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_biblio';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_biblio';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_biblio';
  $strongarm->value = '1';
  $export['node_preview_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_biblio';
  $strongarm->value = 0;
  $export['node_submitted_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_biblio';
  $strongarm->value = '';
  $export['page_title_type_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_biblio_showfield';
  $strongarm->value = 0;
  $export['page_title_type_biblio_showfield'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_biblio_pattern';
  $strongarm->value = '';
  $export['pathauto_node_biblio_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_node_biblio';
  $strongarm->value = 0;
  $export['webform_node_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_biblio';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_biblio'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_biblio';
  $strongarm->value = array(
    'status' => '1',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_biblio'] = $strongarm;

  return $export;
}
