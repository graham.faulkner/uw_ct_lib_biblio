<?php
/**
 * @file
 * uw_ct_lib_biblio.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_lib_biblio_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_geographic_locations|node|biblio|form';
  $field_group->group_name = 'group_geographic_locations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'biblio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Geographic locations',
    'weight' => '63',
    'children' => array(
      0 => 'field_geographic_location',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Geographic locations',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-geographic-locations field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_geographic_locations|node|biblio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_local_catalogue_links|node|biblio|form';
  $field_group->group_name = 'group_local_catalogue_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'biblio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Local catalogue links',
    'weight' => '60',
    'children' => array(
      0 => 'field_local_catalogue_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Local catalogue links',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-local-catalogue-links field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_local_catalogue_links|node|biblio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_worldcat_catalogue_links|node|biblio|form';
  $field_group->group_name = 'group_worldcat_catalogue_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'biblio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'WorldCat catalogue links',
    'weight' => '61',
    'children' => array(
      0 => 'field_worldcat_catalogue_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'WorldCat catalogue links',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-worldcat-catalogue-links field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_worldcat_catalogue_links|node|biblio|form'] = $field_group;

  return $export;
}
