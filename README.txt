INTRODUCTION
------------
This module is a customization of the Biblio module
(https://www.drupal.org/project/biblio) used for displaying scholarly
publications.

The customizations include the addition of three fields to the default Biblio
content type:

* Local library catalogue links
* WorldCat catalogue links
* Geolocation entries, using the Location field type


REQUIREMENTS
------------
This module requires the following modules:
* Biblio, including BibTex, CrossRef, MARC, PM, RIS, RTF, Tagged, and XML
  related modules
* Fieldgroup
* Location
* Node
* Schemaorg
* Search_config
* Text
* UW Web Page
* UW Dashboard FDSU
* Views


INSTALLATION
------------
* Clone the code from https://git.uwaterloo.ca/graham.faulkner/uw_ct_lib_biblio.
* Activate the Library Biblio module on your site as per your normal procedures,
  and confirm you wish to install the Biblio-related modules when prompted.


CONFIGURATION
-------------
* Configure Biblio to taste, using the "Biblio configuration" link in the site
  manager's dashboard submenu.


MAINTAINERS
-----------
Current maintainers:
* Graham Faulkner - graham.faulkner@uwaterloo.ca


TODO:
---------------------
* Customization to CSS/JS to provide better Biblio search experience.
