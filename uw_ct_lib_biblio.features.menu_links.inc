<?php
/**
 * @file
 * uw_ct_lib_biblio.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_lib_biblio_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/config/content/biblio
  $menu_links['management:admin/config/content/biblio'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/content/biblio',
    'router_path' => 'admin/config/content/biblio',
    'link_title' => 'Biblio settings',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure default behavior of the Biblio module.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/config/content',
  );
  // Exported menu link: menu-site-management:admin/config/content/biblio
  $menu_links['menu-site-management:admin/config/content/biblio'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/content/biblio',
    'router_path' => 'admin/config/content/biblio',
    'link_title' => 'Biblio configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure the Waterloo Library Biblio module.',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: navigation:bibliography
  $menu_links['navigation:bibliography'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'bibliography',
    'router_path' => 'bibliography',
    'link_title' => 'Bibliography',
    'options' => array(),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: navigation:node/add/biblio
  $menu_links['navigation:node/add/biblio'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/biblio',
    'router_path' => 'node/add/biblio',
    'link_title' => 'Biblio',
    'options' => array(
      'attributes' => array(
        'title' => 'Use Biblio for scholarly content, such as journal papers and books.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Biblio');
  t('Biblio configuration');
  t('Biblio settings');
  t('Bibliography');


  return $menu_links;
}
